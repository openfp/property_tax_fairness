

#Load Packages ------------------------------------------------------------------------
#Add all required lists to the list.of.packages object
#missing packages will be automatically installed
list.of.packages <- c("feather",
                      "tidyverse",
                      "jsonlite",
                      "here")
new.packages <- list.of.packages[!(list.of.packages %in% installed.packages()[,"Package"])]
if (length(new.packages)) install.packages(new.packages)

#load all required packages
lapply(list.of.packages, require, character.only = TRUE)


options(scipen = 999) #turn off scientific notation

# see working directory
here()

# Import data ---------------------------------------------------------

#README on this data: https://github.com/OrvilleChomer/OpenFP/blob/master/README.md
#data scraped property tax data in JSON
scraped_data_JSON <- fromJSON("https://raw.githubusercontent.com/OrvilleChomer/OpenFP/master/generatedDataFiles/fpPropTaxData.json", flatten = T)



# Extract relevant df -----------------------------------------------------------------------------

# Extract the dataframe with data on Forest Park properites
pin_data <- scraped_data_JSON$pinData
pin_data$clean_pin <- gsub("-", "", pin_data$pin)

# Remove duplicate rows (entire row must be duplicated)
pin_data <- distinct(pin_data)

# Extract the dataframe with data on Forest Park properites that returned missing or wrong
missing_pins <- scraped_data_JSON$missingPins
missing_pins$clean_pin <- gsub("-", "", missing_pins$pin)

#wrong_pins <- scraped_data_JSON$wrongPins


FP_pins <- unique(na.omit(c(pin_data$clean_pin, missing_pins$clean_pin)))


#dups?
dup_pins <- pin_data %>%
      group_by(clean_pin) %>%
      filter(n() > 1) %>% .$clean_pin

ifelse(length(dup_pins) > 1, warning("DUPLICATE pins remain", call. = F), "No DUPLICATE pins")

#Add Variables ------------------------------------------------------------------------------------

#### 
#Add PIN features
#### 
#Breakdown PIN structure
##https://www.cookcountyclerk.com/service/about-property-index-number-pin
pin_data$pin_AREA <- substr(pin_data$pin, start = 1, stop = 2)
pin_data$pin_SUBAREA <- substr(pin_data$pin, start = 4, stop = 5)
pin_data$pin_BLOCK <- substr(pin_data$pin, start = 7, stop = 9)


#### 
#### Add Class descriptions
#### 
#Source: https://www.cookcountyassessor.com/assets/forms/classcode.pdf

#NOTE -- excel would have stored codes as dates so had to store as both code and description and split here
codes <- read.csv(here("Tasks","import","hand","CCA_code_descriptions.csv"), stringsAsFactors = F, na.strings = "")
codes$code <- substr(codes$Code_Description, start = 1, stop = 4)

for (i in 1:nrow(codes)) {
      
      codes[i,"description"] <- gsub(codes[i,"code"], "", codes[i, "Code_Description"])
}

#trim leading and trailing spaces
codes[, c("code","description")] <- sapply(codes[, c("code","description")] , stringr::str_trim)

#add code decriptions to pins_data
pin_data <- merge(pin_data, codes %>% select(code, description, Major_Class, Category, level_of_assessment),
                  by.x = "propCls", by.y = "code", all.x = T)

#### 
#Add property taxes total for 2017 and 2018
#### 
#2017 and 2018 equalizers: https://www2.illinois.gov/rev/research/news/Pages/2018-Cook-County-Final-Multiplier-Announced.aspx
equalizer_2017 <- 2.9627
equalizer_2018 <- 2.9109

#w/homeowner exemption
pin_data$proptax_2017 <- pin_data$taxYrInfoAssVal * equalizer_2017 * (pin_data$propTxRt/100)
pin_data$proptax_2018 <- pin_data$taxYrInfoAssVal * equalizer_2018 * (pin_data$propTxRt/100)


# Import address points data --------------------------------------------------------------------

#README on this data: https://github.com/OrvilleChomer/OpenFP/blob/master/README.md
#Source: https://hub-cookcountyil.opendata.arcgis.com/datasets/5ec856ded93e4f85b3f6e1bc027a2472_0/data

address_points <- read.csv(here("Tasks","import","input","Address_Points.csv"), stringsAsFactors = F)

address_points <- address_points %>%
      select(LONGITUDE, LATITUDE, everything())

address_points <- distinct(address_points)

#Find dups by PIN
address_pin_dups <- address_points %>%
      group_by(PIN) %>%
      filter(n() > 1) %>% .$PIN

#Document that they were dups
address_points$IS_Address_Points_df_PIN_DUP <- ifelse(address_points$PIN %in% address_pin_dups, TRUE, FALSE)


#MUST REVIEW
#Keep only 1 unique pin -- looks like duplicate pins are for same building different unit -- so the 
#geographic info will be slightly off
address_points <- address_points[!duplicated(address_points$PIN), ]




# Join address_points to the scraped data -------------------------------------------
FP_properties <- merge(pin_data, address_points, 
                       by.x = "clean_pin",
                       by.y = "PIN",
                       all = T)


number_of_address_points_PINS_not_in_pin_data <- sum(!(address_points$PIN %in% pin_data$clean_pin))
#View(FP_properties %>% filter(is.na(propEstVal)))



#Import Cook Open Data from the Assessor-----------------------------------------------------------------
##info on this data: https://datacatalog.cookcountyil.gov/stories/s/p2kt-hk36


#import model data
#source w data dictionary: https://datacatalog.cookcountyil.gov/Property-Taxation/Cook-County-Assessor-s-Modeling-Data/5pge-nu6u
##"We use Model Data to estimate a wide range of predictive models that help us characterize 
##home values in a given area. We then select the best performing models to use to value properties in Assessment Data."


destfile <- here("Tasks","import","input","Cook_County_Assessor_s_Modeling_Data.csv")

#If you do NOT already have this LARGE file, download it from the Cook County data portal then write it
if (!file.exists(destfile)) {

      #Will take a few minutes to download
      model_df <- read_csv("https://datacatalog.cookcountyil.gov/api/views/5pge-nu6u/rows.csv?accessType=DOWNLOAD", 
                                    progress = T)
      write.csv(model_df, destfile, row.names = F)
      
}

model_df <- read_csv(destfile)

#Filter by FP pins
model_df_fp <- model_df %>%
      filter(PIN %in% FP_properties$clean_pin)

# #dups
# model_df_fp <- model_df_fp %>%
#       group_by(PIN) %>%
#       filter(n()>1)


#Dups due to multiple sales within time frame of dataset
model_df_fp <- model_df_fp %>% 
      filter(`Most Recent Sale` == 1)


model_df_fp <- distinct(model_df_fp)

#Identify dups -- looks like dups are due to multi-unit properties
#REMOVE until we figure out how to rectify duplicate PINS
model_df_dups_DO_NOT_INCLUDE <- model_df_fp %>%
      group_by(PIN) %>%
      filter(n() > 1) %>% .$PIN


#Join Cook Open Data from the Assessor-----------------------------------------------------------------

FP_properties <- merge(FP_properties, model_df_fp %>% filter(!(PIN %in% model_df_dups_DO_NOT_INCLUDE)),
                       by.x = "clean_pin", by.y = "PIN", all.x = T)


#Export draft processed df
write.csv(FP_properties, here("Tasks","import","output","OpenFP_properties_data_df.csv"), row.names = F)
write_feather(FP_properties, here("Tasks","import","output","OpenFP_properties_data_df.feather"))



# #Cook County Assessor Open Data -- NORTHERN TRI only, DOES NOT INCLUDE FP
# #NOTE -- CANNOT match pins...
# #import assessment data
# #Source w data dictionary: https://datacatalog.cookcountyil.gov/Property-Taxation/Cook-County-Assessor-s-Assessment-Data/bcnq-qi2z
# ##"Where Model Data is a data set of sales, Assessment Data is a data set of properties, even ones 
# ##that have not sold in a long time. Because these properties still need to be valued, we use the best 
# ##performing models from Model Data to estimate the market value for the properties contained within the Assessment Data table."
# 
# assessment_df <- read_csv("Tasks/import/input/Cook_County_Assessor_s_Assessment_Data_100919.csv")
# 
# assessment_df_fp <- assessment_df %>%
#       filter(PIN %in% FP_pins)
# 
# View(assessment_df %>% filter(grepl("^15", PIN)))
# 
# 
# #import first-pass data
# #Source w data dictionary: https://datacatalog.cookcountyil.gov/Property-Taxation/Cook-County-Assessor-s-First-Pass-Values/x88m-e569
# ##First Pass Values are the values upon which re-assessment notices are based. They are the product 
# ##of our modeling process and post-modeling adjustments. 
# 
# firstpass_df <- read_csv("Tasks/import/input/Cook_County_Assessor_s_First_Pass_Values.csv")
# 
# firstpass_df_fp <- firstpass_df %>%
#       filter(PIN %in% FP_pins)
# 
# 





