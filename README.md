# Property Tax Fairness in Forest Park, IL

### Purpose

This is a project of [OpenFP](https://docs.google.com/document/d/1PHoIGtHnVzQeRvG1WxarER2Xl70PSKRbl05j7_aqDG0)

Using an [all our ideas wiki-survey](http://allourideas.org/FPquestions/results?all=true), we asked fellow Forest Parkers: Which question about Forest Park is more important?

The second highest ranked question was "how fair are property taxes?" and the purpose of this project is to use open data to help answer this question.

 
### Notes on [the data](https://gitlab.com/openfp/property_tax_fairness/tree/master/Tasks/import/output)

The data currently comes from three sources:

1. All Forest Park property tax info scraped from the [Cook County Property Tax Portal](http://cookcountypropertyinfo.com/default.aspx). For more information, see [Orville Chomer's github page](https://github.com/OrvilleChomer/OpenFP/blob/master/README.md)


2. Address Points from [Cook Central](https://hub-cookcountyil.opendata.arcgis.com/datasets/5ec856ded93e4f85b3f6e1bc027a2472_0/data). This dataset provides more geographic information for each pin and allowed. For more information, see [Orville Chomer's github page](https://github.com/OrvilleChomer/OpenFP/blob/master/README.md)


3. [Modeling data from the Cook County Assessor](https://datacatalog.cookcountyil.gov/Property-Taxation/Cook-County-Assessor-s-Modeling-Data/5pge-nu6u) used in their Computer Assisted Mass Appraisal system. This dataset contains a lot of features on residential properties sold since 2013.


##### NOTE: This repo aims to be consistent with the [folder structure and workflow created by Patrick Ball at the Human Rights Data Analysis Group (HRDAG).](https://hrdag.org/2016/06/14/the-task-is-a-quantum-of-workflow/)
